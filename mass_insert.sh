#!/bin/bash
for FILE in $HOME/temperature_data/*.csv; do
    echo $FILE
    echo -e ".mode csv\n.import $FILE tmp_temperatures" | sqlite3 temperature_historical.db
    sqlite3 temperature_historical.db < update_db.sql
    rm $FILE
done
