compilation of basic weather data for Los Angeles, CA from the NOAA database, ranging 1950-01-01 to 2017-12-31. Included scripts for database creation and table creation.
