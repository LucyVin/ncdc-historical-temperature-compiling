CREATE TABLE IF NOT EXISTS temperatures (
    TMIN,
    TMAX,
    STATION,
    NAME,
    DATE);

CREATE UNIQUE INDEX IF NOT EXISTS station_date ON temperatures (STATION, DATE);

INSERT INTO temperatures (
	station
	, name
	, DATE
	, tmax
	, tmin
	) select
		station
		, name
		, date
		, tmax
		, tmin
	from tmp_temperatures
		where tmax <> ''
		and tmin <> '';
		
DROP TABLE tmp_temperatures;
